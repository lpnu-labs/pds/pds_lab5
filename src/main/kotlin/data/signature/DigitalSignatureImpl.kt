package data.signature

import data.cipher.CustomCipher
import data.hashing.md5.HashCalculator

class DigitalSignatureImpl(
    private val hashCalculator: HashCalculator,
    private val cipher: CustomCipher
) : DigitalSignature {

    override fun createSignature(message: ByteArray, publicKey: ByteArray): ByteArray {
        val messageHash = hashCalculator.getHash(message)
        return cipher.encrypt(messageHash, publicKey)
    }

    override fun verifySignature(message: ByteArray, signature: ByteArray, privateKey: ByteArray): Boolean {
        val decryptedHash = cipher.decrypt(signature, privateKey)
        val messageHash = hashCalculator.getHash(message)

        return decryptedHash.contentEquals(messageHash)
    }

}