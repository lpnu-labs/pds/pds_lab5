package data.signature

interface DigitalSignature {

    fun createSignature(message: ByteArray, publicKey: ByteArray): ByteArray

    fun verifySignature(message: ByteArray, signature: ByteArray, privateKey: ByteArray): Boolean

}