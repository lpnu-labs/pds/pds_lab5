package data.ext

fun ByteArray.toIntLittleEndianArray(): IntArray {
    val sizeExtra = if (size % 4 > 0) 1 else 0

    val newSize = size / 4 + sizeExtra

    return IntArray(newSize) { index ->
        read4BytesToIntLittleEndian(this, index * 4)
    }
}