package data.ext

fun write4BytesToBuffer(
    buffer: ByteArray,
    offset: Int,
    data: Int,
) {
    buffer[offset + 0] = (data shr 24 and 0xff).toByte()
    buffer[offset + 1] = (data shr 16 and 0xff).toByte()
    buffer[offset + 2] = (data shr 8 and 0xff).toByte()
    buffer[offset + 3] = (data shr 0 and 0xff).toByte()
}

fun write4BytesToBufferLittleEndian(
    buffer: ByteArray,
    offset: Int,
    data: Int,
) {
    buffer[offset + 3] = (data shr 24 and 0xff).toByte()
    buffer[offset + 2] = (data shr 16 and 0xff).toByte()
    buffer[offset + 1] = (data shr 8 and 0xff).toByte()
    buffer[offset + 0] = (data shr 0 and 0xff).toByte()
}

fun read4BytesToIntLittleEndian(
    data: ByteArray,
    offset: Int
) : Int {
    var result = 0

    repeat(4) { index ->
        if (offset + index >= data.size) return result
        result += data[offset + index].toInt() and 0xFF shl index * Byte.SIZE_BITS
    }

    return result
}

val Int.to32bitString: String
    get() = Integer.toBinaryString(this).padStart(Int.SIZE_BITS, '0')