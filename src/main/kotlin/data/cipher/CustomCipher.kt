package data.cipher

interface CustomCipher {
    fun encrypt(data: ByteArray, keyword: ByteArray): ByteArray

    fun decrypt(data: ByteArray, keyword: ByteArray): ByteArray
}