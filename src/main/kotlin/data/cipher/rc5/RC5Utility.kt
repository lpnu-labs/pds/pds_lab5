package data.rc5

import java.math.BigInteger
import generator.LinearGenerator
import java.util.ArrayList

class RC5Utility(private val sizeOfPartInBit: Int, private val roundsNumber: Int) {
    private val sizeOfPartInByte: Int
    var initVector: ByteArray
    private fun divisionIntoParts(data: ByteArray): LongArray {
        val tmp = LongArray(2)
        var n = data.size
        val nB = n / 2
        val nA = n - nB
        var j: Int
        val a = ByteArray(sizeOfPartInBit)
        val b = ByteArray(sizeOfPartInBit)
        n--
        j = sizeOfPartInBit - 1
        for (i in 0 until nB) {
            b[j] = data[n]
            n--
            j--
        }
        j = sizeOfPartInBit - 1
        for (i in nA - 1 downTo 0) {
            a[j] = data[n]
            n--
            j--
        }
        tmp[0] = BigInteger(a).toLong()
        tmp[1] = BigInteger(b).toLong()
        return tmp
    }

    private fun assemblingParts(a: Long, b: Long): ByteArray {
        val tmp = ByteArray(2 * sizeOfPartInBit)
        val byteA = BigInteger("" + a).toByteArray()
        val byteB = BigInteger("" + b).toByteArray()
        //        byteA = byteA;
//
//        byteB = byteB;
        for (i in 0 until sizeOfPartInByte) {
            tmp[i] = byteA[i]
            tmp[sizeOfPartInByte + i] = byteB[i]
        }
        return tmp
    }

    private fun encryptBlock(data: ByteArray, key: RC5Key): ByteArray {
        var a: Long
        var b: Long
        var number: Int
        val s = key.words
        val parts = divisionIntoParts(data)
        a = parts[0]
        b = parts[1]
        /*_____________________________________*/a = a + s[0]
        b = b + s[1]
        for (i in 1..roundsNumber) {
            a = a xor b
            number = (b % sizeOfPartInBit).toInt()
            a = java.lang.Long.rotateLeft(a, number)
            a = a + s[2 * i]
            b = b xor a
            number = (a % sizeOfPartInBit).toInt()
            b = java.lang.Long.rotateLeft(b, number)
            b = b + s[2 * i + 1]
        }
        /*_____________________________________*/
        return assemblingParts(a, b)
    }

    private fun decryptBlock(data: ByteArray, key: RC5Key): ByteArray {
        var a: Long
        var b: Long
        var number: Int
        val s = key.words
        val parts = divisionIntoParts(data)
        a = parts[0]
        b = parts[1]
        /*_____________________________________*/for (i in roundsNumber downTo 1) {
            number = (a % sizeOfPartInBit).toInt()
            b = b - s[2 * i + 1]
            b = java.lang.Long.rotateRight(b, number)
            b = b xor a
            number = (b % sizeOfPartInBit).toInt()
            a = a - s[2 * i]
            a = java.lang.Long.rotateRight(a, number)
            a = a xor b
        }
        a = a - s[0]
        b = b - s[1]
        /*_____________________________________*/
        return assemblingParts(a, b)
    }

    private fun divisionIntoBlocks(data: ByteArray): List<ByteArray> {
        val n = data.size
        val sizeOfBlock = 2 * sizeOfPartInByte
        val divBlock = if (n > sizeOfBlock) n % sizeOfBlock else sizeOfBlock - n
        if (divBlock != 0) {
            println("Data size must be a multiple of $sizeOfBlock")
        }
        val numbersOfBlocks = n / sizeOfBlock
        var tmp: ByteArray
        var counter = 0
        val parts: MutableList<ByteArray> = ArrayList()
        for (i in 0 until numbersOfBlocks) {
            tmp = ByteArray(sizeOfBlock)
            for (j in 0 until sizeOfBlock) {
                tmp[j] = data[counter]
                counter++
            }
            parts.add(tmp)
        }
        return parts
    }

    private fun assemblyOfBlocks(blocks: List<ByteArray>): ByteArray {
        val sizeOfBlock = 2 * sizeOfPartInByte
        val n = blocks.size * sizeOfBlock
        val outputData = ByteArray(n)
        var counter = 0
        for (block in blocks) {
            for (i in 0 until sizeOfBlock) {
                outputData[counter] = block[i]
                counter++
            }
        }
        return outputData
    }

    fun encrypt(data: ByteArray, key: RC5Key): ByteArray {
        val inputBlocks = divisionIntoBlocks(data)
        val outputBlocks: MutableList<ByteArray> = ArrayList()
        var tmp: ByteArray
        val prevBlock = initVector.clone()
        for (block in inputBlocks) {
            tmp = encryptBlock(block, key)
            //tmp = encryptBlock(bitOperation.xorTwoBlocks(prevBlock, block), key);
            for (i in 0..7) {
                prevBlock[i] = tmp[i]
            }
            outputBlocks.add(tmp)
        }
        return assemblyOfBlocks(outputBlocks)
    }

    fun decrypt(data: ByteArray, key: RC5Key): ByteArray {
        val inputBlocks = divisionIntoBlocks(data)
        val outputBlocks: MutableList<ByteArray> = ArrayList()
        var tmp: ByteArray
        val prevBlock = initVector.clone()
        for (block in inputBlocks) {
            tmp = decryptBlock(block, key)
            //tmp = decryptBlock(bitOperation.xorTwoBlocks(prevBlock, block), key);
            for (i in 0..7) {
                prevBlock[i] = tmp[i]
            }
            outputBlocks.add(tmp)
        }
        return assemblyOfBlocks(outputBlocks)
    }

    init {
        sizeOfPartInByte = sizeOfPartInBit / 8
        val tmpLong = LinearGenerator().generateSequence(8)
        val tmp = IntArray(8)
        for (i in tmpLong.indices) {
            tmp[i] = tmpLong[i].toInt()
        }
        initVector = ByteArray(8)
        for (i in tmp.indices) {
            initVector[i] = tmp[i].toByte()
        }
    }
}