package data.rc5

import data.cipher.CustomCipher

class RC5(
    private val wordSize: Int,
    private val rounds: Int,
) : CustomCipher {

    private val rc5Utility: RC5Utility = RC5Utility(wordSize, rounds)

    override fun encrypt(data: ByteArray, keyword: ByteArray): ByteArray {
        return rc5Utility.encrypt(appendAddition(data), createKey(keyword))
    }

    override fun decrypt(data: ByteArray, keyword: ByteArray): ByteArray {
        val decryptedData = rc5Utility.decrypt(data, createKey(keyword))
        return removeAddition(decryptedData)
    }

    private fun appendAddition(data: ByteArray): ByteArray {
        val blockLengthBytes = 2 * wordSize / Byte.SIZE_BITS

        val additionLength = blockLengthBytes - data.size % blockLengthBytes

        val additionByte = additionLength.toByte()

        val padding = ByteArray(additionLength) { additionByte }
        return data + padding
    }

    private fun removeAddition(data: ByteArray): ByteArray {
        val lastByte = data.lastOrNull() ?: return data
        val paddingLength = lastByte.toInt()

        if (paddingLength > data.size) {
            return data
        }

        val isPaddingDetected = data.asSequence().drop(data.size - paddingLength).all { it == lastByte }

        if (!isPaddingDetected) {
            return data
        }

        return data.copyOfRange(0, data.size - paddingLength)
    }

    private fun createKey(keyword: ByteArray) = RC5Key(keyword, wordSize, rounds)

}