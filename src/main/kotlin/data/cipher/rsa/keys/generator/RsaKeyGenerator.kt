package data.cipher.rsa.keys.generator

import data.cipher.rsa.keys.RsaKeys

interface RsaKeyGenerator {

    fun generateKeys(): RsaKeys

}