package data.cipher.rsa.keys

import java.security.KeyFactory
import java.security.PrivateKey
import java.security.PublicKey
import java.security.spec.PKCS8EncodedKeySpec
import java.security.spec.X509EncodedKeySpec

class RsaKeyUtils {

    private val keyFactory: KeyFactory by lazy {
        KeyFactory.getInstance("RSA")
    }

    fun decodePublicKey(key: ByteArray): PublicKey = keyFactory.generatePublic(X509EncodedKeySpec(key))

    fun decodePrivateKey(key: ByteArray): PrivateKey = keyFactory.generatePrivate(PKCS8EncodedKeySpec(key))

    fun encodePublicKey(key: PublicKey): ByteArray = key.encoded

    fun encodePrivateKey(key: PrivateKey): ByteArray = key.encoded

}