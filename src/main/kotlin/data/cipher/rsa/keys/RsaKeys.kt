package data.cipher.rsa.keys

class RsaKeys(
    val public: ByteArray,
    val private: ByteArray,
)