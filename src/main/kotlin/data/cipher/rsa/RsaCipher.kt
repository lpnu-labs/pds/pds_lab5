package data.cipher.rsa

import data.cipher.CustomCipher
import data.cipher.rsa.keys.RsaKeyUtils
import java.util.*
import javax.crypto.Cipher

class RsaCipher(
    private val keyUtils: RsaKeyUtils,
) : CustomCipher {

    companion object {
        private const val RSA_TRANSFORMATION = "RSA/ECB/PKCS1Padding"
    }

    private val cipher: Cipher by lazy {
        Cipher.getInstance(RSA_TRANSFORMATION)
    }

    override fun encrypt(data: ByteArray, keyword: ByteArray): ByteArray {
        cipher.init(Cipher.ENCRYPT_MODE, keyUtils.decodePublicKey(keyword))
        return Base64.getEncoder().encode(cipher.doFinal(data))
    }

    override fun decrypt(data: ByteArray, keyword: ByteArray): ByteArray {
        cipher.init(Cipher.DECRYPT_MODE, keyUtils.decodePrivateKey(keyword))
        return cipher.doFinal(Base64.getDecoder().decode(data))
    }

}