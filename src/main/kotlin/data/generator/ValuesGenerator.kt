package generator

interface ValuesGenerator {

    fun generateValue(): Long

    fun generateSequence(size: Int): List<Long>

    fun reset()

}