package generator

import kotlin.math.pow

class LinearGenerator @JvmOverloads constructor(
    private val initialValue: Long = 13,
    private val multiplier: Int = 14f.pow(3).toInt(),
    private val increment: Int = 2584,
    private val modulus: Int = 2f.pow(27).toInt() - 1,
) : ValuesGenerator {

    private var lastValue: Long = initialValue

    override fun generateValue(): Long {
        return calculateValue(lastValue).also { lastValue = it }
    }

    override fun generateSequence(size: Int): List<Long> {
        return List(size) { generateValue() }
    }

    override fun reset() {
        lastValue = initialValue
    }

    private fun calculateValue(previousValue: Long): Long {
        return (multiplier * previousValue + increment) % modulus
    }

}