package data.hashing.md5

interface HashCalculator {

    fun getHash(message: ByteArray): ByteArray

    fun getHashString(message: ByteArray): String

}