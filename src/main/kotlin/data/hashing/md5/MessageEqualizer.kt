package data.hashing.md5

import data.ext.write4BytesToBuffer
import data.ext.write4BytesToBufferLittleEndian

class MessageEqualizer {

    fun equalizeMessage(message: ByteArray): ByteArray {
        val paddingSize = calculatePaddingAndLengthSize(message.size)
        val padding = ByteArray(paddingSize)

        appendLengthAndPadding(padding, paddingSize - Byte.SIZE_BITS,  message.size)

        return message + padding
    }


    private fun calculatePaddingAndLengthSize(messageSize: Int): Int {
        val sizeOfMessageInLastBlock = messageSize % 64

        var paddingAndLengthSize = (64 - sizeOfMessageInLastBlock)
        if (paddingAndLengthSize <= 8) paddingAndLengthSize += 64

        return paddingAndLengthSize
    }


    private fun appendLengthAndPadding(padding: ByteArray, paddingSize: Int, messageSize: Int) {
        padding[0] = 128u.toByte() // 1000 0000
        for (i in 1 until paddingSize) {
            padding[i] = 0
        }
        write4BytesToBufferLittleEndian(
            buffer = padding,
            offset = padding.size - 8,
            data = messageSize * Byte.SIZE_BITS,
        )
        write4BytesToBuffer(
            buffer = padding,
            offset = padding.size - 4,
            data = 0,
        )
    }

}