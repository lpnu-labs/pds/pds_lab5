package data.hashing.md5

import data.ext.*
import data.utils.toHexStringUppercase

class Md5Calculator : HashCalculator {

    override fun getHash(message: ByteArray): ByteArray {
        val messageWithPadding = MessageEqualizer().equalizeMessage(message)

        val integerMessage = messageWithPadding.toIntLittleEndianArray()

        return processMessageIn16WordBlocks(integerMessage)
    }

    override fun getHashString(message: ByteArray): String {
        return getHash(message).toHexStringUppercase()
    }

    private fun processMessageIn16WordBlocks(data: IntArray): ByteArray {
        val wordsBuffer = MD5WordsBuffer()

        for (block in data.indices step 16) {
            val previousA = wordsBuffer.a
            val previousB = wordsBuffer.b
            val previousC = wordsBuffer.c
            val previousD = wordsBuffer.d

            processBlock(data, wordsBuffer, block)

            wordsBuffer.a += previousA
            wordsBuffer.b += previousB
            wordsBuffer.c += previousC
            wordsBuffer.d += previousD
        }

        return createHashArray(wordsBuffer)
    }

    private fun processBlock(data: IntArray, buffer: MD5WordsBuffer, currentBlockIndex: Int) {
        repeat(64) { j ->
            processRound(data, buffer, currentBlockIndex, j)
        }
    }

    private fun processRound(data: IntArray, buffer: MD5WordsBuffer, currentBlockIndex: Int, roundNumber: Int) {
        var blockIndex = 0
        var f = 0
        when (roundNumber) {
            in 0..15 -> {
                f = f(buffer.b, buffer.c, buffer.d)
                blockIndex = roundNumber
            }
            in 16..31 -> {
                f = g(buffer.b, buffer.c, buffer.d)
                blockIndex = (5 * roundNumber + 1) % 16
            }
            in 32..47 -> {
                f = h(buffer.b, buffer.c, buffer.d)
                blockIndex = (3 * roundNumber + 5) % 16
            }
            in 48..63 -> {
                f = i(buffer.b, buffer.c, buffer.d)
                blockIndex = (7 * roundNumber) % 16
            }
        }
        f += buffer.a + K[roundNumber] + data[currentBlockIndex + blockIndex]

        buffer.a = buffer.d
        buffer.d = buffer.c
        buffer.c = buffer.b
        buffer.b += Integer.rotateLeft(f, getShiftValue(roundNumber))
    }

    private fun getShiftValue(roundNumber: Int): Int {
        val roundGroupNumber = roundNumber / 16
        val roundSubgroupNumber = roundNumber % 4

        return SHIFTING[roundGroupNumber * 4 + roundSubgroupNumber]
    }

    private fun createHashArray(wordsBuffer: MD5WordsBuffer): ByteArray {
        val hashArray = ByteArray(16)

        write4BytesToBufferLittleEndian(hashArray, 0, wordsBuffer.a)
        write4BytesToBufferLittleEndian(hashArray, 4, wordsBuffer.b)
        write4BytesToBufferLittleEndian(hashArray, 8, wordsBuffer.c)
        write4BytesToBufferLittleEndian(hashArray, 12, wordsBuffer.d)

        return hashArray
    }
}