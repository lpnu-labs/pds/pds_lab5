package data.hashing.md5

import kotlin.math.abs
import kotlin.math.sin

const val WORD_A = 0x67452301L.toInt()
const val WORD_B = 0xEFCDAB89L.toInt()
const val WORD_C = 0x98BADCFEL.toInt()
const val WORD_D = 0x10325476L.toInt()

val K: IntArray by lazy {
    IntArray(64) { i -> (abs(sin(i + 1.0)) * (1L shl 32)).toLong().toInt() }
}

val SHIFTING by lazy {
    intArrayOf(
        // First round
        7, 12, 17, 22,
        // Second round
        5, 9, 14, 20,
        // Third round
        4, 11, 16, 23,
        // Fourth round
        6, 10, 15, 21
    )
}