package data.hashing.md5

fun f(x: Int, y: Int, z: Int): Int {
    return (x and y) or (x.inv() and z)
}

fun g(x: Int, y: Int, z: Int): Int {
    return (x and z) or (y and z.inv())
}

fun h(x: Int, y: Int, z: Int): Int {
    return x xor y xor z
}

fun i(x: Int, y: Int, z: Int): Int {
    return y xor (x or z.inv())
}
