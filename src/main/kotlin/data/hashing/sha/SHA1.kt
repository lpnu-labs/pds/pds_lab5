package data.hashing.sha

import data.hashing.md5.HashCalculator
import data.utils.toHexStringUppercase
import java.security.MessageDigest

class SHA1 : HashCalculator {

    private val messageDigest: MessageDigest by lazy { MessageDigest.getInstance("SHA-1") }

    override fun getHash(message: ByteArray): ByteArray {
        return messageDigest.digest(message)
    }

    override fun getHashString(message: ByteArray): String {
        return getHash(message).toHexStringUppercase()
    }
}