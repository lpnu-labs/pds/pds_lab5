package app

import data.cipher.CustomCipher
import data.cipher.rsa.RsaCipher
import data.cipher.rsa.keys.RsaKeyUtils
import data.cipher.rsa.keys.generator.RsaKeyGenerator
import data.cipher.rsa.keys.generator.RsaKeyGeneratorImpl
import data.hashing.md5.HashCalculator
import data.hashing.sha.SHA1
import data.signature.DigitalSignature
import data.signature.DigitalSignatureImpl
import data.utils.toHexStringUppercase
import java.io.File

class AppExecutor {

    companion object {
        private const val INPUT_FILE = "input_file.txt"

        private const val SIGNATURE_FILE = "signature.txt"
    }

    private val hashCalculator: HashCalculator = SHA1()

    private val cipher: CustomCipher
    private val keysGenerator: RsaKeyGenerator

    init {
        val utils = RsaKeyUtils()
        cipher = RsaCipher(utils)
        keysGenerator = RsaKeyGeneratorImpl(utils, 2048)
    }

    private val digitalSignature: DigitalSignature = DigitalSignatureImpl(hashCalculator, cipher)

    fun execute() {
        executeForFiles()

        executeForString()
    }

    private fun executeForFiles() {
        println("--------------------------------------- Executing for file ---------------------------------------")

        val message = readFile(INPUT_FILE)

        executeForMessage(message)
    }

    private fun executeForString() {
        println("--------------------------------------- Executing for String ---------------------------------------")

        val message = "Hello, it's test message!!!!".toByteArray()

        executeForMessage(message)
    }

    private fun executeForMessage(message: ByteArray) {
        val keys = keysGenerator.generateKeys()

        val signature = digitalSignature.createSignature(message, keys.public)

        println("Signature: ${signature.toHexStringUppercase()}")
        writeFile(SIGNATURE_FILE, signature.toHexStringUppercase())

        val loadedSignature = readFileString(SIGNATURE_FILE).decodeHex()
        val isSignatureValid = digitalSignature.verifySignature(message, loadedSignature, keys.private)

        println("Is signature valid: $isSignatureValid")
    }

    private fun readFile(name: String): ByteArray {
        return File(name).readBytes()
    }

    private fun readFileString(name: String): String {
        return File(name).readText()
    }

    private fun writeFile(name: String, data: String) {
        return File(name).writeText(data)
    }

    fun String.decodeHex(): ByteArray {
        check(length % 2 == 0) { "Must have an even length" }

        return chunked(2)
            .map { it.toInt(16).toByte() }
            .toByteArray()
    }

}